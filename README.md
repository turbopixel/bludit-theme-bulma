> In progress!

# bluma theme

A [bulma](https://github.com/jgthms/bulma) css based theme for [bludit](https://github.com/bludit/bludit).

## Installation

**Git clone**

* Connect to the server via ssh
* Move to the bludit directory (e.g. `/var/www/bludit/bl-themes/)`
* Clone this repository `git clone git@codeberg.org:turbopixel/bludit-theme-bulma.git`
* Activate bluma in the administration area

**Direct download**

* Download the theme archive zip file
* Upload the files into the bulma/bl-theme/ directory
* Activate bluma in the administration area

## Preview

![](https://codeberg.org/turbopixel/bludit-theme-bulma/raw/branch/master/preview/bluma-theme-demo.png)

## Changelog

** comming soon **
